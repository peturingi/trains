package eu.petur.trains;

import org.testng.annotations.Test;

import org.testng.Assert;

public class WaypointTest {

  @Test
  public void testCanStop() throws Exception {
    final Waypoint stoppable = new Waypoint("Test", true);
    Assert.assertTrue(stoppable.canStop() == true, "Waypoint should be stoppable.");

    final Waypoint notStoppable = new Waypoint("Test", false);
    Assert.assertTrue(notStoppable.canStop() == false, "Waypoint should not be stoppable.");
  }

  @Test(expectedExceptions = NullPointerException.class)
  void testIdNotNull() throws Exception {
    new Waypoint(null, true);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  void testIdNotEmpty() throws Exception {
    new Waypoint("", true);
  }
}
