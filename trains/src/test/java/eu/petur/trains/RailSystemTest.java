package eu.petur.trains;

import org.testng.annotations.Test;

import java.net.URL;

import static org.testng.Assert.*;

public class RailSystemTest {
  @Test
  public void testValidNetworkAndRoute() throws Exception {
    final URL networkConfig = this.getClass().getResource("/Network1");
    final URL routeConfig = this.getClass().getResource("/TrainRoutes1");
    final RailSystem railSystem = new RailSystem(networkConfig, routeConfig);
  }
}