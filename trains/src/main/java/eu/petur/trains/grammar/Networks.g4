grammar Networks;

id : STRING;

station : 'STAT' STRING id;

connect : 'CONN' id id;

end     : 'END' id;

networks : (station | connect | end)*
        | '#' STRING // Comments
        ;

STRING : ([a-z]|[A-Z]|[0-9])+ ;

WS  :   [ \t\n\r]+ -> skip;
