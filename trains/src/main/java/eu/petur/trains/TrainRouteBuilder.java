package eu.petur.trains;

import eu.petur.trains.gen.TrainRoutes.TrainRoutesLexer;
import eu.petur.trains.gen.TrainRoutes.TrainRoutesParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public final class TrainRouteBuilder {
  public static Route build(final URL configuration) throws IOException {
    Validate.notNull(configuration, "Configuration was null.");
    final FileInputStream inputStream;
    final File file = new File(configuration.getFile());
    inputStream = new FileInputStream(file);
    final ANTLRInputStream input = new ANTLRInputStream(inputStream);
    final TrainRoutesLexer lexer = new TrainRoutesLexer(input);
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    final TrainRoutesParser parser = new TrainRoutesParser(tokens);
    final ParseTree tree = parser.routes();
    final TrainRoutesBuilderVisitor visitor = new TrainRoutesBuilderVisitor();
    visitor.visit(tree);
    inputStream.close();
    return visitor.getRoute();
  }
}
