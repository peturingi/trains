package eu.petur.trains;

import eu.petur.trains.gen.TrainRoutes.TrainRoutesBaseVisitor;
import eu.petur.trains.gen.TrainRoutes.TrainRoutesParser;
import org.antlr.v4.runtime.misc.NotNull;

/**
 * TrainRoutesBuilderVisitor builds a <code>Route</code> from a parse tree.
 * The parse tree is a representation of the TrainRoutes DSL.
 *
 * @see Route
 * @see Waypoint
 */
public final class TrainRoutesBuilderVisitor extends TrainRoutesBaseVisitor<String> {

  /**
   * Generic visitors are expected to return something.
   * This visitors functionality does not depend up on its return value; it is never used.
   */
  static private final String EMPTY = "";

  /**
   * The route being built by this visitor.
   */
  private Route route = new Route();

  /**
   * Gets the complete route built by this visitor.
   *
   * @return The route built by this visitor. The list is empty if the route contains no waypoints.
   */
  public Route getRoute() {
    return route;
  }

  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>route</i> non-terminal and its children.
   */
  @Override
  public String visitRoutes(@NotNull TrainRoutesParser.RoutesContext ctx) {
    for (int i = 0; i < ctx.getChildCount(); i++)
      ctx.getChild(i).accept(this);
    return EMPTY;
  }


  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>stop</i> non-terminal and its children.
   */
  @Override
  public String visitStop(@NotNull TrainRoutesParser.StopContext ctx) {
    route.addWaypoint(ctx.id().getText(), true);
    return EMPTY;
  }


  /**
   * <p><b>This method is for use by an ANTLR generated parser; do not invoke this method directly.</b></p>
   * Visit the <i>id</i> non-terminal and its children.
   */
  @Override
  public String visitId(@NotNull TrainRoutesParser.IdContext ctx) {
    route.addWaypoint(ctx.getText(), false);
    return EMPTY;
  }
}
