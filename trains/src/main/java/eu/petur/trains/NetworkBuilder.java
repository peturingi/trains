package eu.petur.trains;

import eu.petur.trains.gen.Networks.NetworksLexer;
import eu.petur.trains.gen.Networks.NetworksParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Set;

public final class NetworkBuilder {

  public static Set<Track> build(final URL configuration) throws IOException {
    Validate.notNull(configuration, "Configuration value cannot be null.");


    final FileInputStream inputStream;
    final File file = new File(configuration.getFile());
    inputStream = new FileInputStream(file);
    final ANTLRInputStream input = new ANTLRInputStream(inputStream);
    final NetworksLexer lexer = new NetworksLexer(input);
    final CommonTokenStream tokens = new CommonTokenStream(lexer);
    final NetworksParser parser = new NetworksParser(tokens);
    final ParseTree tree = parser.networks();

    final NetworkBuilderVisitor visitor = new NetworkBuilderVisitor();
    visitor.visit(tree);
    inputStream.close();
    return visitor.getTracks();
  }
}