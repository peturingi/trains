package eu.petur.trains.gen.Networks;// Generated from /Users/eu.petur/DTU/RS/rs-code/src/eu/eu.petur/Networks.g4 by ANTLR 4.5
import eu.petur.trains.gen.Networks.NetworksParser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link NetworksParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface NetworksVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link NetworksParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(@NotNull NetworksParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworksParser#station}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStation(@NotNull NetworksParser.StationContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworksParser#connect}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConnect(@NotNull NetworksParser.ConnectContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworksParser#end}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnd(@NotNull NetworksParser.EndContext ctx);
	/**
	 * Visit a parse tree produced by {@link NetworksParser#networks}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNetworks(@NotNull NetworksParser.NetworksContext ctx);
}