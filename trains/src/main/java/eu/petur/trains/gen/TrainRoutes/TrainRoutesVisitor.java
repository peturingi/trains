package eu.petur.trains.gen.TrainRoutes;// Generated from /Users/petur/DTU/RS/java-code/trains/src/main/java/eu/petur/trains/grammar/TrainRoutes.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TrainRoutesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TrainRoutesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TrainRoutesParser#routes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoutes(@NotNull TrainRoutesParser.RoutesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TrainRoutesParser#stop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStop(@NotNull TrainRoutesParser.StopContext ctx);
	/**
	 * Visit a parse tree produced by {@link TrainRoutesParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(@NotNull TrainRoutesParser.IdContext ctx);
}