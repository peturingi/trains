package eu.petur.trains;

/**
 * A <code>Waypoint</code> is a <code>Point</code> on a <code>Route</code>.
 * In the domain of a train route, a waypoint is a point in space at which a train can stop.
 *
 * @see Point
 */
public final class Waypoint extends Point {

  /**
   * Can a train stop at this waypoint?
   */
  private final boolean canStop;

  /**
   * Class constructor specifying this points identifier and whether a train may stop at this waypoint.
   *
   * @param id a unique identifier.
   * @param canStop true if a train is allowed to stop at this waypoint; false otherwise.
   * @throws NullPointerException <code>id</code> was null.
   * @throws IllegalArgumentException <code>id</code> was empty.
   */
  public Waypoint(final String id, final boolean canStop) {
    super(id);
    this.canStop = canStop;
  }

  /**
   * Gets whether a train may stop at this waypoint.
   *
   * @return true if a train may stop at this waypoint; false otherwise.
   */
  public boolean canStop() {
    return canStop;
  }
}
