package eu.petur.trains;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

/**
 * <code>Point</code> is the class representing an <i>point</i>.
 * A point connects <code>track</code>s.
 * Two <code>End</code> tracks may not be connected to the same point.
 * A <code>Station</code> track may be connected two times to the same point.
 *
 * @see End
 * @see Station
 * @see Switch
 * @see Track
 */
public class Point {
  /**
   *  The identifier for this point.
   *  Identifiers are unique.
   */
  private String id;

  /**
   * The <code>Track</code>s this point conncets.
   */
  private List<Track> connectedTracks = new ArrayList<Track>();

  /**
   * Class constructor specifying an identifier.
   *
   * @param id a unique identifier.
   * @throws IllegalArgumentException <code>id</code> was null.
   * @throws IllegalArgumentException <code>id</code> was empty.
   */
  public Point(final String id) {
    Validate.notNull(id, "Identifier was null.");
    Validate.isTrue(StringUtils.isNotEmpty(id), "Identifier was empty.");
    this.id = id;
  }

  /**
   * Connect this point to a track.
   *
   * @param track a track which this point connects to.
   * @throws IllegalArgumentException <code>track</code> was null.
   * @throws IllegalArgumentException the point already holds a connection to the given track.
   * @throws IllegalArgumentException if the <code>track</code> was not connected correctly.
   */
  public void connectTrack(final Track track) {
    Validate.notNull(track, "The track value cannot be null.");
    final int oldConnectionsCount = this.connectionCount();
    if (connectedTracks.contains(track))
      throw new IllegalStateException("This point has already been connected with track.");
    if (track instanceof Station)
      for (Track t: getConnectedTracks())
        if (t instanceof Station && t != track)
          throw new IllegalArgumentException("This point already holds a connection with a station");
    connectedTracks.add(track);
    final int newConnectionCount = this.connectionCount();
    Validate.isTrue(newConnectionCount == oldConnectionsCount + 1, "Track was not added correctly");
    Validate.isTrue(newConnectionCount <= 3 && newConnectionCount > 0, "Track was not added correctly");
  }

  /**
   * Disconnect this point from a track.
   *
   * @param track a track which this point connects to.
   * @throws IllegalArgumentException <code>track</code> was null.
   * @throws IllegalArgumentException this point does not hold a connection to <code>track</code>.
   * @throws IllegalArgumentException if the <code>track</code> was not disconnected correctly.
   */
  public void disconnectTrack(final Track track) {
    Validate.notNull(track, "The track value cannot be null.");
    Validate.isTrue(connectedTracks.contains(track), "The provided track is not connected to this point.");
    final int oldConnectionsCount = this.connectionCount();
    connectedTracks.remove(track);
    final int newConnectionCount = this.connectionCount();
    Validate.isTrue(newConnectionCount == oldConnectionsCount - 1, "Track was not removed correctly");
  }

  /**
   * Gets the number of tracks this point is connected to.
   *
   * @return number of tracks this point is connected to.
   */
  public int connectionCount() { return connectedTracks.size(); }

  /**
   * Gets this tracks identifier.
   *
   * @return this tracks identifier.
   */
  public String getId() { return id; }

  /**
   * Gets all tracks this point connects to.
   *
   * @return tracks this point connects to.
   */
  public List<Track> getConnectedTracks() {
    return Collections.unmodifiableList(connectedTracks);
  }

  /**
   * Checks if the point has a station connected to it.
   *
   * @return true if the point has a station in its list of connected objects, false otherwise
   */
  public boolean hasStationConnected() {
    final List<Track> tracks = this.getConnectedTracks();
    for(Track t : tracks) {
    	if (t instanceof Station) {
          return true;
      }
    }
    return false;
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other)
      return true;
    if (!(other instanceof Point))
      return false;
    return StringUtils.equals(id, ((Point) other).getId());
  }

  /**
   * Intended only for debugging.
   *
   * @return This points identifier.
   */
  @Override
  public String toString() {
    return id;
  }
}
