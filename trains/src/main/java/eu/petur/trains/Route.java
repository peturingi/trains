package eu.petur.trains;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;

/**
 * <code>Route</code> represents the path of way points a train is to travel on a network.
 *
 * A route must be complete.
 * A route is complete if each of its way points is connected to track which is connected
 * to the next point in the routes list of way points.
 * A route can be acyclic or cyclic.
 * A cyclic route has the same identifier as its first and last way point.
 * A acyclic route must begin at a point which is connected to a track on which a train can stop.
 *
 * @see Track
 * @see Point
 * @see Waypoint
 */
public final class Route {
  /**
   * The points along this route.
   */
  private List<Waypoint> points = new LinkedList<Waypoint>();

  /**
   * Add a new point to this route.
   *
   * @param id the identifier of a waypoint within the network this route belongs to.
   * @param stop indication of whether the train following this route should stop at the point identified by <code>id</code>.
   * @throws IllegalArgumentException <code>id</code> was null.
   * @throws IllegalArgumentException <code>id</code> was empty.
   * @see Station
   */
  public void addWaypoint(final String id, final boolean stop) {
    Validate.notNull(id, "Identifier was null");
    Validate.isTrue(StringUtils.isNotEmpty(id), "Identifier was empty");
    // TODO We can not add a point two times in a row; it is okay to add the point multiple times as long as other points are between its instances.
    points.add(new Waypoint(id, stop));
  }

  /**
   * Get all points along this route.
   *
   * @return points along this route.
   */
  public List<Waypoint> waypoints() {
    return Collections.unmodifiableList(points);
  }
}
