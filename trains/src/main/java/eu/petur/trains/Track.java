package eu.petur.trains;

import org.apache.commons.lang3.Validate;

import java.util.*;

/**
 * <code>Track</code> is a connection between points.
 *
 * @see Point
 */
public abstract class Track {

  /**
   * The points this track connects.
   */
  protected List<Point> points = new LinkedList<Point>();

  /**
   * Get the number of points this track connects.
   *
   * @return the number of points connected by this track.
   */
  protected int connectedPoints() {
    return points.size();
  }

  /**
   * Connect this track to a point.
   *
   * @param point the point this track is to connect to.
   * @throws IllegalArgumentException <code>point == null</code>.
   */
  public void connectTo(final Point point) {
    Validate.notNull(point, "The point cannot be null.");
    points.add(point);
    assert points.contains(point);
  }

  /**
   * Disconnect this track from a point.
   *
   * @param point the point this track is to disconnect from.
   * @throws IllegalArgumentException <code>point == null</code>.
   */
  public void disconnectFrom(final Point point) {
    Validate.notNull(point, "The point cannot be null.");
    Validate.isTrue(points.contains(point), "The track is not connected to the provided point.");
    points.remove(point);
  }

  /**
   * Get the points this track connects to.
   *
   * @return the points this track connects to.
   */
  public List<Point> connectedToPoints() {
    return Collections.unmodifiableList(points);
  }

  /**
   * Evaluates whether a track leads from a given point to a given point.
   *
   * @param fromId the id of the point this track leads from
   * @param toId the id of the point this track leads to
   * @return true if the track leads from a point to a point; false otherwise.
   * @throws NullPointerException either fromId or toId are null.
   * @throws IllegalArgumentException if either fromId or toId are empty.
   */
  public boolean leads(final String fromId, final String toId) {
    Validate.notNull(fromId, "ID cannot be null.");
    Validate.notNull(toId, "ID cannot be null.");
    Validate.notEmpty(fromId, "The ID has to contain at least one character.");
    Validate.notEmpty(toId, "The ID has to contain at least one character.");

    boolean from = false;
    boolean to = false;
    for (Point p : connectedToPoints()) {
      if (p.getId().equals(fromId))
        from = true;
      else if (p.getId().equals(toId))
        to = true;
      }
    return from && to;
  }

  /**
   * Intended only for debugging.
   *
   * @return The points this track is connected to.
   */
  @Override
  public String toString() {
    return connectedToPoints().toString();
  }
}
