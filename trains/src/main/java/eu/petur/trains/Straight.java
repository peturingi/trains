package eu.petur.trains;

/**
 * A <i>straight track</i> connects two points.
 */
public class Straight extends Track {
  // TODO I believe this is unused. Remove if truely unused.
  protected boolean isStation = false; // A straight track can be a station, but default is false. DSL does not offer it marked as station.

  /**
   * Connect this straight track to a point.
   * @param point the point this track is to connect to.
   * @throws IllegalStateException tried to connect to more than 2 points.
   * @throws IllegalStateException tried to connect again to a previous point.
   */
  @Override
  public void connectTo(final Point point) {
    if (connectedToPoints().contains(point))
      throw new IllegalStateException("Can not connect to a previously connected point.");
    if (connectedPoints() == 2)
      throw new IllegalStateException("Can not connect more than 2 points.");
    super.connectTo(point);
  }
}
