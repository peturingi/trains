package eu.petur;

import eu.petur.trains.*;

import java.io.IOException;
import java.util.logging.*;

/*
* TRACKS:
* (done) Straight track connects two points.
* (done) Switch track connects three points.
* (done) End track must not be connected to another end track.
* (done) All points must be connected to tracks.
* (done) Multiple tracks connected together, must come to an end.
*
* ROUTES:
* (done) First point on a route must have a station.
* (done) Train routes must be complete in that they mention all tracks used (error in specification, they must mention all points with an exception of the midpoint in a branch).
* (done) A mentioned track must be in forward direction, unless current track can be stopped on.
* (done) In a switch, trains are not always free to choose what to visit (switches have a sense of direction).
*
* EXTENSIONS (Think about the following, do not implement!)
* How to ensure trains do not collide (two trains on same track).
* How to check that routes do not collide.
* How to avoid collisions by rescheduling (delaying) trains, but they will still visit all tracks on their route.
*
*/

public final class Main {

    private final static Logger logger = Logger.getLogger(Main.class.getPackage().getName());

    public static void main(final String[] args) {
      try {
          //TODO: Usability: These could come from user input (e.g. args)
          final String networkConfiguration = "/Configurations/Valid/Network2";
          final String routeConfiguration = "/Configurations/Valid/TrainRoutes2";
          final String logFile = "trains.log";
          final Level level = Level.ALL;
          final boolean append = false;

          configureLogger(logFile, level, append);

          logger.log(Level.INFO, "Creating rail system. Network configuration: " + networkConfiguration + ". Route configuration: " + routeConfiguration + ".");
          final RailSystem railSystem = new RailSystem(Main.class.getClass().getResource(networkConfiguration), Main.class.getClass().getResource(routeConfiguration));
          logger.log(Level.INFO, "Rail system successfully created.");
          System.exit(0);
      } catch (Exception e) {
          logger.log(Level.SEVERE, "Exception caught", e);
          System.exit(1);
      }
    }

    /**
     * Configures logger ouptut file, append policy and logger level.
     * Levels: SEVERE - Exceptions, INFO - Most important messages, FINE - Execution of top level tasks, FINER - Completion of top level tasks, FINEST - debug messages
     * @param path the location of the file
     * @param level the desired logger level
     * @param append whether the logger should append messages (true) to the file or start fresh (false)
     * @throws IOException if there are IO problems with the file
     */
    private static void configureLogger(final String path, final Level level, final boolean append) throws IOException{
        final Handler handler = new FileHandler(path, append);
        final SimpleFormatter formatter = new SimpleFormatter();
        handler.setLevel(level);
        handler.setFormatter(formatter);
        logger.setLevel(level);
        logger.addHandler(handler);
    }
}